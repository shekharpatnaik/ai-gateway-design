# AI Gateway and AI Agent Design

## Table of Contents

- [Why AI Gateway?](#why-ai-gateway)
- [What are AI Agents?](#what-are-ai-agents)
- [Architecture Overview](#architecture-overview)

## Why AI Gateway

[AI Gateway](https://docs.gitlab.com/ee/architecture/blueprints/ai_gateway/) acts as the glue between GitLab Monolith and the LLMs. The main motivation is to enable teams to create AI features for the GitLab quickly, easily and securely. It gives us the following advantages:

1. Gives us a platform that enables us to iterate quickly.
2. Allows us to decouple third party dependencies such as LLMs from the Monolith.
3. Uses `python` as the primary programming language that is the most common language used in GenAI libraries and applications such as [LangChain](https://www.langchain.com/), [Semantic Router](https://github.com/aurelio-labs/semantic-router/tree/main), [Lite LLM](https://litellm.ai/), etc. In addition, using python allows us to use a number of PyTorch and Tensorflow models within the pipeline.


## What are AI Agents

[GitLab AI Agents](https://about.gitlab.com/direction/ai-powered/ai_framework/ai_agents/) allow us to create programmatic agents on the GitLab platform. Agents communication with LLMs in a cyclical way, passing in tools that the LLM can use to get user and context specific information. Agents will be leveraged as a platform by AI Gateway in order to store, version and managed prompts and tools. This will allow us to iterate quickly and allow GitLab and Community members to easily develop and deliver AI features.

> These are very rough screen examples to illustrate what configuration is possible for AI Agents

![](images/AI_Agent_create_screen.png)

All agents will be created on the GitLab UI and the monolith will be the source of truth for Agent configuration. In this example we create an Agent called `Duo Chat for Issues`. An LLM and Model will be selected for the AI Agent. The model configuration is stored in the AI Gateway i.e. which models are available and therefore, a request will go from rails to the AI Gateway to list available models. Various LLM specific parameters can be configured for such as temperature, seed, stop sequence, etc.

Users will be able to configure a `System Prompt` as well as a `User Prompt` for the agent. The prompts can be templated using [Jinja](https://jinja.palletsprojects.com/en/3.1.x/). In addition to this developers / users will be able to enable one or more tools that can be used with the agent. When the prompt is sent to the LLM, it will include tool information. 

Agents will be versioned, so that we can experiment with different prompts by changing simple configuration parameters in the gateway. This will also allow us to do canaries and try out red/green deployments of prompts.

In addition, users can test AI agents in a chat like interface on the UI, where they can enter certain context parameters such as record ID, question, etc to start the agent conversation in order to test behavior.

All of the new features we build will be modeled as Agents. Including:

1. Duo Chat
2. Code Generation
3. Code Suggestions?
4. Explain this Vulnerability

### Tool Configuration

Tools allow AI Agents to perform actions and query GitLab data. In the future these tools can be extended to also get data from outside of GitLab. Tools will be configured in the GitLab UI. 

The following screen example shows what the tool configuration would look like:

![](images/AI_Agent_tools_screen.png)

The user will create a tool with a name and select an executor type. Tools can be either executed in Rails and call a rails module such as Search Documentation, Fetch Issue, etc or would be executed in Python for latency sensitive requirements such as intent detection, tree-sitter based indentation, etc. Tools inputs will be defined generically. In addition, where does the tool need to run - whether it should run before sending to the LLM or post LLM response. Pre-processing tools will always run and will have a sequence. Examples of pre-processing tools are, documentation fetch or issue fetch, where based on the semantic meaning of the route we can pre-fetch information to send to the LLM in order to save time. Post-processing tools would in include - code echo detection, issue fetch, documentation lookup etc.

Once the tool has been executed, we will specify a tool response template, that would take the output from running the tool, substitute values and append the output to the user response.

### Rails API

AI Agents will expose a rails API, that will be used to fetch the prompt information given an agent name and version.

Example Request

```json
{
    "agent_name": "Duo Chat - Issues",
    "version": "v1"
}
```

> Note: version can also be passed as `latest` which will fetch the latest version.


Example Response:
```json
{
    "llm": "anthropic",
    "model": "claude-2.1.",
    "temperature": 0.1,
    "system_prompt": "You are a helpful GitLab assistant",
    "prompt": "{{.question}}",
    "python_tools": [{
        "name": "code_echo_check",
        "inputs": "{{.suffix}}"
    }]
}
```

## Architecture Overview

We need to be able to run this service reliably on GitLab.com and support the following platforms:

1. Gitlab.com with AI Gateway managed by GitLab with GitLab managed Anthropic and Google Models
2. Self Managed with AI Gateway managed by GitLab with GitLab managed Anthropic and Google Models
3. Self Managed with AI Gateway managed by Customer with customer managed models
4. Self Managed with AI Gateway with Customer managed API keys for Anthropic and Google

![](images/AI_Gateway_Architecture.png)


[Cells](https://docs.gitlab.com/ee/architecture/blueprints/cells/) are an important part of the future state architecture of GitLab and therefore this design assumes that Rails and AI Gateway will be run in the context of a cell. Each cell will have a co-located AI Gateway.

The IDE will always directly hit the AI gateway. Certain routes would require tools and therefore hit the GitLab monolith.

The IDE will first hit the AI Firewall. The firewall will be responsible for [PII removal](https://github.com/microsoft/presidio), [Prompt injection detection](https://huggingface.co/protectai/deberta-v3-base-prompt-injection) and [secrets detection](https://github.com/gitleaks/gitleaks).

After the request goes through the firewall, we will detect which agent to use based on the routing configuration.

Routes in the AI Gateway will be defined with configuration. Here is an example:

```yaml
port: 5001
gitlab_url: "http://127.0.0.1:3000/"

observability:
  snowplow_enabled: true
  log_level: debug

routes:
- name: Duo Chat 
  path: /api/v1/chat
  routes:
  - agent: Duo Chat - Issues
    version: v1
    utterances:
    - Can you describe issue 324 for me?
    - Can you please summarize this issue?
    - How would I fix this issue?
  - agent: Code Generation
    version: latest
    utterances:
    - Can you write a python program to print the first 10 odd numbers?
    - Can you refactor this code for me?
    - Can you write tests for the following code block?
  - agent: Generic Agent
    version: latest 
```

> Note: In the case above, the Generic Agent will be executed as the default case when none of the utterances yield a high enough score with the input question.

Based on the path selected, we will use [Semantic Router](https://github.com/aurelio-labs/semantic-router/tree/main) to either statically or dynamically route to paths. If the prompt for the selected route is not in cache (memory for the first iteration) then the prompt information will be fetched from GitLab using the agent API (described above).

Then depending on the Agent, we will execute the pre-processing tasks / tools and populate the templates with values. These tasks would run in python or in some cases in rails.

The request will then be sent to the LLM. We will use [LiteLLM](https://litellm.ai/) to call the LLM API. LiteLLM will be imported as a python library and allows us access many LLMs using a consistent API.

After the response is returned and if it contains a function calls, then we will execute a rails API that will execute the tools for a given agent.

If no functions need to be called then the response is returned to the user.

**Note:** Self managed instances will always need to deploy AI Gateway. In the case when the keys are managed by us then we will route the request to a edge service that will inject keys and call the LLMs that are managed by us.
