# AI Gateway v2 POC

This is a POC to experiment with what the next iteration of AI Gateway could look like.


## Development Environment Setup

### Prerequisites

Python and [Poetry](https://python-poetry.org/)
OpenAI and Anthropic API keys configured. (Will change from OpenAI to local embeddings in the future)

### Setup dependencies

```sh
poetry install
```

### Run components

First run the Registry UI

```sh
poetry shell
streamlit run agent_registry/Agents.py
```

Next we need to run the Registry API

```sh
poetry shell
python ./agent_registry/api.py
```

Finally we run AI Gateway v2

```sh
poetry run ai_gateway
```


To test the API you can run
```sh
curl -X POST \
  -H 'Content-Type: application/json' \
  -d @./samples/solve_issue.json \
  http://localhost:5001/api/v1/chat | jq
```
