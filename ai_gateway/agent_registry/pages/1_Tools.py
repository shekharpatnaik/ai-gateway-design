import streamlit as st
import db
import pandas as pd

st.header("Tools")
tools_df = db.get_all_tools()

columns_to_display = ['name', 'type', 'executor_type', 'module']
tools_df = tools_df[columns_to_display]

st.dataframe(
    tools_df,  
    column_config={
            "name": "Name",
            "type": "Type",
            "executor_type": "Executor",
            "module": "Module"
    },
    use_container_width=True)

