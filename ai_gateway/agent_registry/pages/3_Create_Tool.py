import streamlit as st
import db

st.header("Create Tool")

name = st.text_input(label="Tool Name")
executor_type = st.selectbox(label="Executor Type", options=["Python", "Rails"])
module = st.text_input(label="Module")
tool_type = st.selectbox(label="Type", options=["Pre-Processing", "Response"])
response_template = st.text_area(label="Response Template")
schema = st.text_area(label="Schema")
submit = st.button(label="Create", type="primary")
if submit:
    db.create_tool(db.Tool(
        name=name, 
        executor_type=executor_type if executor_type else "",
        module=module,
        tool_schema=schema,
        tool_type=tool_type if tool_type else "",
        response_template=response_template
    ))
