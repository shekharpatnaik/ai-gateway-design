import streamlit as st 
import db

llm_to_model_mapping = { 
    "Anthropic": ["claude-2.1", "claude-3-haiku-20240307", "claude-3-sonnet-20240229", "claude-3-opus-20240229"],
    "Google": ["code-bison-002", "gemini-1.5"],
    "Llama2": ["code-llama", "llama2-instruct"],
    "Ollama": ["ollama/codellama"]
}

st.set_page_config(layout="wide")

col1, col2 = st.columns([3, 2])

with col1:
    agent_name = st.text_input(label="Agent Name")
    llm = st.selectbox(
        label="LLM", options=llm_to_model_mapping.keys())
    models = llm_to_model_mapping[llm] if llm else []
    model = st.selectbox(
        label="Model", 
        options=models)
    temperature = st.slider(label="Temperature", min_value=0.0, max_value=1.0, step=0.1, value=0.2)
    system_prompt = st.text_area(label="System Prompt")
    all_tools = db.get_all_tools()
    tools = st.multiselect(label="Tools", options=all_tools[["name", "id"]])
    submitted = st.button(label="Create New Version", type="primary")
    if submitted:
        if agent_name == "" or llm == "" or model == "" or system_prompt == "":
            st.write("Please complete all fields")
        else:
            agent = db.create_agent_if_not_exists(db.Agent(name=agent_name))
            latest_version = db.get_new_version(agent_name)
            db.insert_new_version(
                    agent_name, 
                    db.AgentVersion(
                        version=latest_version, 
                        llm=llm if llm else "",
                        model=model if model else "", 
                        system_prompt=system_prompt, 
                        temperature=temperature),
                    tools
            )
            st.toast(body='New version created', icon='😍')


with col2:
    st.write('Test Agent')
    prompt = st.chat_input("Say something....")
