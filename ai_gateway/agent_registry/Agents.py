import streamlit as st
import db
import pandas as pd

st.header("Agents")
agents_df = db.get_all_agents()
agent_versions_df = db.get_all_agent_versions()
merged_df = pd.merge(agents_df, agent_versions_df, left_on='id', right_on='agent_id', how='right')
columns_to_display = ['name', 'version', 'llm', 'model']
filtered_df = merged_df[columns_to_display]

st.dataframe(
    filtered_df,  
    column_config={
        "name": "Name",
        "version": "Version",
        "llm": "LLM"
    },
    use_container_width=True)

