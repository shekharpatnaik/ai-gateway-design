import sqlite3
import pandas as pd
from typing import Optional, List
from sqlalchemy import text

from pydantic import BaseModel
import streamlit as st

conn = st.connection('registry', 'sql')

def setup():
    conn = sqlite3.connect("agent_registry.db")
    cur = conn.cursor()
    cur.execute('''DROP TABLE IF EXISTS agents''')
    cur.execute('''DROP TABLE IF EXISTS agent_versions''')
    cur.execute('''DROP TABLE IF EXISTS tools''')
    cur.execute('''DROP TABLE IF EXISTS agent_version_tools''')

    cur.execute('''
        CREATE TABLE IF NOT EXISTS agents (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            latest_version INTEGER
        );
    ''')

    cur.execute('''
        CREATE TABLE IF NOT EXISTS agent_versions (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            agent_id INTEGER,
            version INTEGER,
            llm TEXT,
            model TEXT,
            system_prompt TEXT,
            temperature REAL,
            FOREIGN KEY(agent_id) REFERENCES agents(id)
        );
    ''')

    cur.execute('''
        CREATE TABLE IF NOT EXISTS tools (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            executor_type TEXT,
            module TEXT,
            schema TEXT,
            type TEXT,
            response_template TEXT
        );
    ''')
 
    cur.execute('''
        CREATE TABLE IF NOT EXISTS agent_version_tools (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            agent_version_id INTEGER,
            tool_id INTEGER,
            FOREIGN KEY(agent_version_id) REFERENCES agent_versions(id),
            FOREIGN KEY(tool_id) REFERENCES tools(id)
        );
    ''')

class Agent(BaseModel):
    id: int = -1
    name: str
    latest_version: int = 0

class AgentVersion(BaseModel):
    id: int = -1
    agent_id: int = -1
    version: int
    llm: str
    model: str
    system_prompt: str
    temperature: float

class Tool(BaseModel):
    id: int = -1
    name: str
    executor_type: str
    module: str
    tool_schema: str
    tool_type: str
    response_template: str

def find_agent(name: str) -> Optional[Agent]:
    df = conn.query('SELECT id, name, latest_version FROM agents WHERE name = :name', params=dict(name=name), ttl=0)
    if len(df) == 0:
        return None
    return Agent(id=df.loc[0, 'id'], name=df.loc[0, 'name'], latest_version=df.loc[0, 'latest_version'])

def create_agent_if_not_exists(agent: Agent):
    with conn.session as s:
        existing_agent = find_agent(agent.name)
        if existing_agent:
            return agent

        s.execute(text('''INSERT INTO agents (name, latest_version) VALUES (:name, :version)'''), params=dict(name=agent.name, version=agent.latest_version))
        s.commit()
        return find_agent(agent.name)
    
def get_new_version(name: str) -> int:
    with conn.session as s:
        s.execute(text('''UPDATE agents SET latest_version = latest_version + 1 WHERE name = :name'''), params=dict(name=name))
        s.commit()
        record = find_agent(name)
        if not(record):
            return 1
        return record.latest_version


def insert_new_version(agent_name: str, version: AgentVersion, tools: List[str]):
    with conn.session as s:
        agent = find_agent(agent_name)
        if not(agent):
           return 

        s.execute(
            text('''INSERT INTO agent_versions (agent_id, version, llm, model, system_prompt, temperature) VALUES (:agent_id, :version , :llm, :model, :sprompt, :temp)'''),
            params=dict(agent_id=agent.id, version=version.version, llm=version.llm, model=version.model, sprompt=version.system_prompt, temp=version.temperature))

        s.commit()
        
        ver = conn.query(
                'SELECT Id FROM agent_versions WHERE agent_id = :id AND version = :version',
                ttl=0,
                params=dict(id=agent.id, version=version.version)
        )
        version_id = ver.loc[0, 'id']

        for tool in tools:
            df = conn.query('SELECT Id FROM tools WHERE NAME = :name', params=dict(name=tool), ttl=0)
            s.execute(
                text('''INSERT INTO agent_version_tools (agent_version_id, tool_id) VALUES (:version_id, :tool_id)'''),
                params=dict(version_id=int(version_id), tool_id=int(df.loc[0, 'id']))
            )

        s.commit()

def create_tool(tool: Tool):
    with conn.session as s:
        s.execute(
            text('''
            INSERT INTO tools (name, executor_type, module, schema, type, response_template) 
            VALUES (:name, :etype , :module, :schema, :ttype, :temp)'''),
                params=dict(
                    name=tool.name, 
                    etype=tool.executor_type, 
                    module=tool.module, 
                    schema=tool.tool_schema,
                    ttype=tool.tool_type, 
                    temp=tool.response_template)
        )
        s.commit()


def get_all_agents() -> pd.DataFrame:
    return conn.query('''SELECT * FROM agents''', ttl=0)

def get_all_agent_versions() -> pd.DataFrame:
    return conn.query('''SELECT id, agent_id, version, llm, model, system_prompt, temperature FROM agent_versions ORDER BY agent_id desc, version desc''', ttl=0)

def get_all_tools() -> pd.DataFrame:
    return conn.query('SELECT * FROM tools', ttl=0)

