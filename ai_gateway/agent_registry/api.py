from typing import List
from pydantic import BaseModel
import uvicorn
import sqlite3
from fastapi import FastAPI

app = FastAPI()

class Tool(BaseModel):
    name: str
    executor: str
    module: str
    tool_type: str
    tool_schema: str
    response_template: str

class Response(BaseModel):
    name: str
    version: int
    llm: str
    model: str
    temperature: float
    system_prompt: str
    tools: List[Tool]

@app.get('/api/v1/agent/{name}/version/{version}')
def get_version(name: str, version: int) -> Response:
    conn = sqlite3.connect("agent_registry.db")
    cur = conn.cursor()
    cur.execute('''
        SELECT name, version, llm, model, temperature, system_prompt
        FROM agent_versions
        INNER JOIN agents ON agent_id = agents.id
        WHERE name = ? AND version = ?
    ''', (name, version))
    record = cur.fetchone()

    cur.execute('''
        SELECT tools.name, tools.executor_type, module, type, schema, response_template FROM agent_version_tools
        INNER JOIN agent_versions ON agent_version_id = agent_versions.id
        INNER JOIN tools ON tools.id = tool_id
        INNER JOIN agents ON agent_versions.agent_id = agents.id
        WHERE agents.name = ? and agent_versions.version = ?
    ''', (name, version))

    tool_records = cur.fetchall()
    tools = []
    for tool in tool_records:
        tools.append(Tool(name=tool[0], executor=tool[1], 
                          module=tool[2], tool_type=tool[3], 
                          tool_schema=tool[4], response_template=tool[5]))

    conn.close()
    return Response(name=record[0], version=record[1], llm=record[2],
                    model=record[3], temperature=record[4], system_prompt=record[5], tools=tools)

def main():
    uvicorn.run(app, host='0.0.0.0', port=9111)
    
if __name__ == '__main__':
    main()


