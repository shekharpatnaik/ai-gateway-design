from pydantic import BaseModel
from typing import List
import yaml

class Observability(BaseModel):
    log_level: str
    snowplow_enabled: bool

class SubRoute(BaseModel):
    agent: str
    version: str
    utterances: List[str]

class Route(BaseModel):
    name: str
    path: str
    sub_routes: List[SubRoute]

class Config(BaseModel):
    port: int
    gitlab_url: str
    route_encoder: str
    reload: bool = True

    observability: Observability
    routes: List[Route]

def load_config_from_yaml(file_path: str) -> Config:
    with open(file_path, 'r') as file:
        data = yaml.safe_load(file)
        return Config(**data)

