import httpx
from pydantic import BaseModel
from typing import List

class Tool(BaseModel):
    name: str
    executor: str
    module: str
    tool_type: str
    tool_schema: str
    response_template: str

class Response(BaseModel):
    name: str
    version: int
    llm: str
    model: str
    temperature: float
    system_prompt: str
    tools: List[Tool]

cache = {}

async def fetch_agent_config(agent_name_version: str) -> Response:
    if agent_name_version in cache:
        return cache[agent_name_version]

    [agent_name, version] = agent_name_version.split(":")

    async with httpx.AsyncClient() as client:
        data = await client.get(f'http://localhost:9111/api/v1/agent/{agent_name}/version/{version}')
        response = Response(**data.json())
        cache[agent_name_version] = response
        return response
