import importlib
from typing import Union, Callable
from semantic_router import Route, RouteLayer
from semantic_router.encoders import BaseEncoder

from ai_gateway.config import Route as CfgRoute, Config

MODULE_NAME = "semantic_router.encoders"
module = importlib.import_module(MODULE_NAME)

def get_encoder(cfg: Config) -> BaseEncoder:
    class_ = getattr(module, cfg.route_encoder)
    return class_()

def setup_router(cfg: CfgRoute, encoder: BaseEncoder) -> Union[RouteLayer, Callable]:

    if len(cfg.sub_routes) == 1:
        def setup_default_route(*args):
            sub_route = cfg.sub_routes[0]
            return Route(name=f'{sub_route.agent}:{sub_route.version}', utterances=[])
        return setup_default_route

    routes = []
    for sub_route in cfg.sub_routes:
        rt = Route(
            name=f'{sub_route.agent}:{sub_route.version}', 
            utterances=sub_route.utterances
        )
        routes.append(rt)

    return RouteLayer(encoder=encoder, routes=routes)


