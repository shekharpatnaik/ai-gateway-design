from typing import List

from ai_gateway.request import Message


def add_system_prompt(messages: List[Message], system_prompt_template: str) -> List[Message]:
    result = [Message(role='system', content=system_prompt_template)]
    for msg in messages:
        if msg.role.lower() != 'system':
            result.append(msg)

    return result

