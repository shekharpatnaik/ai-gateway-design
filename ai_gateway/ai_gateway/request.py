from typing import Dict, List, Union
from pydantic import BaseModel
from litellm import Message

# class Message(BaseModel):
#    role: str
#    content: str

class Request(BaseModel):
    messages: List[Message]
    context: Dict[str, str]

