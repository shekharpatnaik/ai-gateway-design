from importlib.resources import contents
import uvicorn
import json
from fastapi import FastAPI
from jinja2 import Template
from typing import cast

from ai_gateway import config, request, router, gitlab, utils
from ai_gateway.tools.tool import execute_tool
from litellm import Choices, Message, ModelResponse, completion
import litellm

app = FastAPI()

def main():
    cfg = config.load_config_from_yaml("./config.yaml") 
    route_encoder = router.get_encoder(cfg)
    # logging.setLevel(cfg.observability.log_level)

    for route in cfg.routes:

        route_layer = router.setup_router(cfg=route, encoder=route_encoder)
        @app.post(route.path)
        async def routeFunction(input: request.Request):
            last_message = input.messages[-1].content

            # Run Semantic Routing
            agent = route_layer(last_message)
            if not(agent.name):
                return

            # Fetch Agent config from GitLab
            agent_config = await gitlab.fetch_agent_config(agent.name) 

            # Add system prompt if it doesn't exist
            messages = utils.add_system_prompt(input.messages, agent_config.system_prompt)

            # Execute Pre-tools
            for tool in agent_config.tools:
                if tool.tool_type == 'Pre-Processing':
                    tool_execution_result = await execute_tool(
                        cfg=cfg, 
                        module=tool.module, 
                        message=last_message, 
                        context=input.context)

                    # Apply result to template
                    template = Template(tool.response_template)
                    tool_message = template.render(tool_execution_result)
                    last_message += "\n" + tool_message
                    messages[-1] = request.Message(content=last_message, role="user")

            # Add tools to request
            llm_tools = []
            for tool in agent_config.tools:
                if tool.tool_type == 'Response':
                    llm_tools.append({
                        "type": "function",
                        "function": {
                            "name": tool.name,
                            "description": "",
                            "parameters": json.loads(tool.tool_schema)
                        }
                    })

            # Call LLM
            called_tools = True
            response = None
            # litellm.set_verbose=True

            while called_tools:
                print(list(map(lambda x: dict(x), messages)))
                response = completion(
                    model=agent_config.model, 
                    messages=list(map(lambda x: dict(x), messages)),
                    max_tokens=4096,
                    tools=llm_tools,
                    stream=False
                )

                response = cast(ModelResponse, response)
                print(f"Response from LLM is {response}")

                # TODO Execute Post-Tools
                called_tools = False
                for choice in response.choices:
                    message = cast(Choices, choice).message
                    
                    if hasattr(message, 'tool_calls'):
                        for tc in message.tool_calls:
                            fn = tc.function.name
                            for tool in agent_config.tools:
                                if tool.tool_type == 'Response' and fn == tool.name:
                                    messages.append({
                                        "role": message.role,
                                        "content": [{
                                            "type": "text",
                                            "text": message.content if message.content else ""
                                        }, {
                                            "type": "tool_use",
                                            "id": tc.id,
                                            "name": fn,
                                            "input": json.loads(tc.function.arguments)
                                        }]
                                    })
                                                    # messages.append(Message(
                                    #     role="assistant",
                                    #     contents=f"Request to run tool {fn} with arguments {tc.function.arguments}" 
                                    # ))
                                    called_tools = True
                                    try:
                                        result = await execute_tool(
                                            cfg=cfg,
                                            module=tool.module,
                                            message=tc.function.arguments,
                                            context=input.context
                                        )
                                        messages.append(Message(tool_call_id=tc.id, role="tool", name=fn, content=result))
                                    except Exception as e:
                                        messages.append(Message(
                                            role="user",
                                            content=f"Error running tool {fn}: {e}"
                                        ))

            return {
                "response": response,
                "context": input.context
            }

    uvicorn.run(app, host='0.0.0.0', port=cfg.port)
    
if __name__ == '__main__':
    main()


