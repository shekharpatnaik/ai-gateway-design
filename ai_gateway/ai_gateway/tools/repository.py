import git
import uuid
import os

from typing import Any, Dict
from os import path

from ai_gateway.config import Config
from ai_gateway.tools.tool import Tool

class RepositoryClone(Tool):
    
    async def execute(self, cfg: Config, message: str, context: Dict[str, str]) -> Any:
        project_url = context.get("project_url", "")
        branch = context.get("branch", "main")

        if project_url == "":
            raise Exception("Project URL is missing in the payload")
        
        temp_dir = path.join("./temp", str(uuid.uuid4()))
        os.makedirs(temp_dir, exist_ok=True)
        
        print(f"Cloning into tempdir {temp_dir}")
        repo = git.Repo.clone_from(project_url, temp_dir)
        repo.git.fetch('origin', branch)
        repo.git.checkout(branch)
        context['clone_directory'] = temp_dir
        print(f"Finished cloning repository")
        return {}


class RepositoryPush(Tool):
    
    async def execute(self, cfg: Config, message: str, context: Dict[str, str]) -> Any:

        dir = context['clone_directory']
        branch = context.get("branch", "main")
        
        repo = git.Repo(dir)

        repo.git.add(".")
        repo.git.commit("-m", "Tanuki Fixit fixing issues")
        try:
            repo.git.push("origin", branch, "-u", f"origin/{branch}", "-f")
        except Exception:
            pass
        print(f"Finished pushing repository")
        return "Finished pushing repository"

