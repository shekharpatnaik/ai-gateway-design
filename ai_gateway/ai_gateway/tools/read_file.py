from os import path
import git
import tempfile

from typing import Any, Dict

from ai_gateway.config import Config
from ai_gateway.tools.tool import Tool
from pydantic import BaseModel

class _Input(BaseModel):
    filename: str

class ReadFile(Tool):
    
    async def execute(self, cfg: Config, message: str, context: Dict[str, str]) -> Any:

        root_dir = context['clone_directory']
        input = _Input.parse_raw(message)
        full_name = path.join(root_dir, input.filename)

        with open(full_name, 'r') as f:
            return f.read()

