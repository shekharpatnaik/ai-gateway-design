import importlib
from abc import ABC, abstractmethod
from typing import Dict, Any

from ai_gateway.config import Config

class Tool(ABC):

    @abstractmethod
    async def execute(self, cfg: Config, message: str, context: Dict[str, str]) -> Any:
        pass


async def execute_tool(cfg: Config, module: str, message: str, context: Dict[str, str]) -> Any:
    # TODO if this a ruby tool then call the API
    [module_name, class_name] = module.split(":")
    mod = importlib.import_module(module_name)
    class_ = getattr(mod, class_name)
    tool_instance = class_()
    return await tool_instance.execute(cfg, message, context)
