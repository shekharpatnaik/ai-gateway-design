from os import getenv
from typing import Any, Dict

import httpx
from ai_gateway.config import Config
from ai_gateway.tools.tool import Tool


class Notes(Tool):
    
    async def execute(self, cfg: Config, message: str, context: Dict[str, str]) -> Any:
        print(f"Context is {context}")
        merge_request_id = context.get("mr_id", "")
        project_id = context.get("project_id", "")
        note_id = context.get("note_id", "")
        
        token = getenv("GITLAB_TOKEN")
        gitlab_url = cfg.gitlab_url

        async with httpx.AsyncClient() as client:
            response = await client.get(
                f'{gitlab_url}/api/v4/projects/{project_id}/merge_requests/{merge_request_id}/notes/{note_id}', 
                headers={
                    "Authorization": f"Bearer {token}"
                })

            return response.json()

