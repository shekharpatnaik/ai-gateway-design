from os import getenv
from typing import Any, Dict

import httpx
from ai_gateway.config import Config
from ai_gateway.tools.tool import Tool


class Issues(Tool):
    
    async def execute(self, cfg: Config, message: str, context: Dict[str, str]) -> Any:
        issue_id = context.get("issue_id", "")
        project_id = context.get("project_id", "")
        if issue_id == "" or project_id == "":
            return {}
        
        token = getenv("GITLAB_TOKEN")
        gitlab_url = cfg.gitlab_url

        async with httpx.AsyncClient() as client:
            response = await client.get(f'{gitlab_url}/api/v4/projects/{project_id}/issues/{issue_id}', headers={
                "Authorization": f"Bearer {token}"
            })

            return response.json()

